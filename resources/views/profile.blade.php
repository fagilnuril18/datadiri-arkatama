<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Diri</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>

<body class="bg-light">
    <main class="container">
        <form action="{{ url('submit') }}" method="POST">
            @csrf

            <div class="my-3 p-3">
                <div class="mb-3 row">
                    <label for="nim" class="col-sm-2 col-form-label">NIM</label>
                    <div class="col-sm-10">
                        <input tabindex="1" type="text" class="form-control" name="nim" id="nim"
                            autocomplete="off">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input tabindex="2" type="text" class="form-control" name="nama" id="nama"
                            autocomplete="off">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="jeniskelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select class="form-select" name="jeniskelamin" id="jeniskelamin">
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                </div>
                
             <div class="mb-3 row">
                    <label for="prodi" class="col-sm-2 col-form-label">prodi</label>
                    <div class="col-sm-10">
                        <input tabindex="3" type="text" class="form-control" name="prodi"id="prodi"
                            autocomplete="off">
                    </div>
                </div>

             <div class="mb-3 row">
                    <label for="fakultas" class="col-sm-2 col-form-label">fakultas</label>
                    <div class="col-sm-10">
                        <input tabindex="3" type="text" class="form-control" name="fakultas" id="fakultas"
                            autocomplete="off">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" name="submit">Simpan</button>
            </div>
        </form>

    </main>

</body>

</html>
