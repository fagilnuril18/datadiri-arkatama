<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mahasiswa extends Model
{
    public $timestamps = false;
    use HasFactory;
    protected $fillable = ['nim', 'nama', 'jk', 'prodi', 'fakultas']; //agar column bisa di isi
    protected $table = 'mahasiswa';
}
