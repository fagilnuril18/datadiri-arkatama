<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\mahasiswa;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
         $request->validate(
            [
                'nim' => 'required',
                'nama' => 'required',
                'jeniskelamin' => 'required',
                'prodi' => 'required',
                'fakultas' => 'required',
            ],
        );
        $data = [
            'nim' => $request->nim, //untuk tabel nim di isi oleh field dengan nama nik
            'nama' => $request->nama,
            'jk' => $request->jeniskelamin,
            'prodi' => $request->prodi,
            'fakultas' => $request->fakultas,
        ];
        mahasiswa::create($data);
        return redirect()->to('index')->with('success', 'Berhasil melakukan CREATE data');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
